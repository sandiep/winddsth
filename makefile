#
#       !!!! Do NOT edit this makefile with an editor which replace tabs by spaces !!!!    
#
##############################################################################################
# 
# On command line:
#
# make all = Create project
#
# make clean = Clean project files.
#
# To rebuild project do "make clean" and "make all".
#

##############################################################################################
# Start of default section

TOOLCHAIN = arm-none-eabi-
CC   = $(TOOLCHAIN)gcc
CP   = $(TOOLCHAIN)objcopy
AS   = $(TOOLCHAIN)gcc -x assembler-with-cpp
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary

MCU  = cortex-m3
MCFLAGS = -mcpu=$(MCU)
C_COMPILER_STD = -std=gnu99

ARCH_C_FLAGS	= -mthumb -mno-thumb-interwork -fno-strict-aliasing -fwrapv -fomit-frame-pointer -fverbose-asm  $(OPT) -DDEBUG_STACK_STAMP=0x5A

#
# End of default section
##############################################################################################

##############################################################################################
# Start of user section
#

# 
# Define Debug mode, project name and Ram/Flash mode here
PRIME_DEBUG = 1
SRCDIR = Src
LOP_DEBUG_BUILD = PRIME_DEBUG
TARGET_OUT_PATH = Build/Hex
OBJ_PATH=Build/Obj


# List C source files here
SRC  =	$(SRCDIR)/windDSTH.c	\
		$(SRCDIR)/eduarmBoardConfig.c \
		$(SRCDIR)/windDSTHInit.c
		
# List ASM source files here
ASRC =

# List all user directories here
UINCDIR =	Include						\
			Include/prime_framework		\
			Include/windDSTH_Helper
			
			
# List the user directory to look for the libraries here
ULIBS = -lprimeFramework -lwindDSTH_Helper

# Define optimisation level here
OPT = -O0

ifeq ($(PRIME_DEBUG),1)
OBJDIR = Build/Obj/Debug
TARGET_OUT = Build/Hex/windDSTH_debug
UDEFS = -DCORE_FAMILY_arm -DMCU_CHIP_lpc1768 -DPRIME_DEBUG
ULIBDIR =	Lib/prime_framework/Debug		\
			Lib/windDSTH_Helper/Debug
			
ASFLAGS = $(MCFLAGS) -g -gdwarf-2 -Wa,-amhls=$(<:.s=.lst)
CPFLAGS = $(MCFLAGS) $(OPT) -gdwarf-2 -mthumb -fomit-frame-pointer -Wall -Wno-cpp -fverbose-asm -Wa,-ahlms=$(<:.c=.lst) $(C_COMPILER_STD) $(DEFS)

else
OBJDIR = Build/Obj/Release
TARGET_OUT = Build/Hex/windDSTH_release
UDEFS =   -DCORE_FAMILY_arm -DMCU_CHIP_lpc1768
ULIBDIR =	Lib/prime_framework/Release		\
			Lib/windDSTH_Helper/Release
			
ASFLAGS = $(MCFLAGS) -Wa,-amhls=$(<:.s=.lst)
CPFLAGS = $(MCFLAGS) $(OPT) -mthumb -fomit-frame-pointer -Wall -Wno-cpp -fverbose-asm -Wa,-ahlms=$(<:.c=.lst) $(DEFS)
endif

LDSCRIPT = lpc1768_flash.ld
FULL_TARGET_OUT = $(TARGET_OUT)
DEFS    = $(UDEFS) -DRUN_FROM_FLASH=1

INCDIR  = $(patsubst %,-I%,$(UINCDIR))
LIBDIR  = $(patsubst %,-L%,$(ULIBDIR))

OBJS = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRC))

LIBS    = $(ULIBS)
LDFLAGS = $(MCFLAGS) -mthumb --specs=nano.specs --specs=nosys.specs -nostartfiles -T$(LDSCRIPT) -Wl,-Map=$(FULL_TARGET_OUT).map,--cref,--no-warn-mismatch $(LIBDIR) 


# Generate dependency information
CPFLAGS += -MD -MP -MF .dep/$(@F).d

#
# makefile rules
#

.PHONY: all
all: $(OBJS) $(FULL_TARGET_OUT).elf $(FULL_TARGET_OUT).hex


$(OBJDIR)/%.o : $(SRCDIR)/%.c
	mkdir -p $(OBJDIR)
	mkdir -p Build/Hex
	$(CC) -c $(CPFLAGS) -I . $(INCDIR) $< -o $@

$(OBJDIR)/%.o : %.s
	$(AS) -c $(ASFLAGS) $< -o $@

%.elf: $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) $(LIBS) -o $@
  
%.hex: %.elf
	$(HEX) $< $@

%.bin: %.elf
	$(BIN) $< $@

.PHONY: clean	
clean:
	-rm -f $(OBJS)
	-rm -rf $(OBJ_PATH)
	-rm -rf $(TARGET_OUT_PATH)
	-rm -f $(SRC:.c=.c.bak)
	-rm -f $(SRC:.c=.lst)
	-rm -f $(ASRC:.s=.s.bak)
	-rm -f $(ASRC:.s=.lst)
	-rm -fR .dep

# 
# Include the dependency files, should be the last of the makefile
#
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)

# *** EOF ***