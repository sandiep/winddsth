@echo off

set CWD=%CD%
REM set this variable to the path where you installed arm tool chain
set ARM_TOOL_CHAIN_PATH=%CWD%\Tools\GNU Tools ARM Embedded\4.8 2014q2\bin

REM set this variable to the path where you extracted the MinGW compiler suite
set MSYS_PATH=%CWD%\Tools\MinGW\msys\1.0\msys.bat

set PATH=%PATH%;%ARM_TOOL_CHAIN_PATH%
set HOME=.

call "%MSYS_PATH%"


