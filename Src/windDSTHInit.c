#include "windDSTHConfig.h"
#include "eduarmBoardDefs.h"
#include "eduarmBoardConfig.h"

void eint3Callback(void);

/*********************************Clock Configuration**********************/
PFCfgClk clkConfig = 
						{	
							100000000,
							12000000,
							enPllClkSrcMainOSC
						};

/*********************************RIT Configuration**********************/
PFCfgRit ritConfig = 
						{
							25000,// configure RIT for 1ms
							0x00,			// compare mask
							pfTickUpdate,	// callback
							enPclkDiv_4,	// pclk divider
							enBooleanTrue,	// halt on break
							enBooleanTrue	// reset on match
						};

/*********************************External interrupt Configuration**********************/

PFcallback eintCallbackArr = { eint3Callback };

PFCfgEint3 eInt3Config = 
{
	&eintCallbackArr,
	1,
	enEint3ModeLowLevel
};						
/*********************************UART0 Configuration**********************/
PFCfgUart0 uart0Config = 
						{
							enPclkDiv_4, 			
							enUart0Baudrate_9600, 	
							enUart0Databits_8, 		
							enUart0ParityNone, 		
							enUart0StopBits_1, 
							enUart0IntRx
						};

/*********************************SPI Configuration**********************/
PFCfgSpi0 spi0Cfg =
						{
							enPclkDiv_4,			// Peripheral clock divider for SPI0 module		
							enBooleanTrue,			// Select master or slave mode for SPI0			
							enSpi0Databits_8,		// Set datasize for SPI0 packet					
							enSpi0Mode_0,			// Select SPI0 mode								
							25000000,				// SPI0 channel baudrate in bits/second`		
							enSpi0IntNone			// Select SPI0 interrupts to enable				
						};
						

PFCfgAdc adcConfig = 		{
													enPclkDiv_4, // clk
													2,  // prescaler
													0, // burst mode channel
													enAdcChannel0, // single mode channel
													0,// callback
													0,// interrupt 
													0, // burst mode
													enAdcStartNone // conversion mode
												};


CfgGfx lcdDisplayConfig  = 
						{  
							{
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_0},
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_1},
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_2},
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_3},
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_4},
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_5},
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_6},
								{EDUARM_LCD_DATA_PORT,EDUARM_LCD_DATA_7}	    
							},//data
							{EDUARM_LCD_CS_PORT,EDUARM_LCD_CS_PIN},//CS
							{EDUARM_LCD_RS_PORT,EDUARM_LCD_RS_PIN},//RS
							{EDUARM_LCD_WR_PORT,EDUARM_LCD_WR_PIN},//WR
							{EDUARM_LCD_RD_PORT,EDUARM_LCD_RD_PIN},//RD
							{EDUARM_LCD_RST_PORT,EDUARM_LCD_RST_PIN},//Reset
							enGfxOrientation_0,
							320,
							240				
						};						
						

CfgTouch touchConfig = 
						{
							{EDUARM_TOUCH_SSP_0_INT_PORT, EDUARM_TOUCH_SSP_0_INT_PIN},
							{EDUARM_TOUCH_SSP_0_BUSY_PORT, EDUARM_TOUCH_SSP_0_BUSY_PIN},
						    {EDUARM_TOUCH_SSP_0_SSEL_PORT, EDUARM_TOUCH_SSP_0_SSEL_PIN},
						    enTouchReferenceSelect_Differential,
						    enTouchPrecision_12bit,
							pfSpi0RegisterDevice,
							pfSpi0ChipSelect,
							pfSpi0Write,
							pfSpi0Read
                        };
						


void boardInit(void)
{
	PFEnStatus status;
	
	//Peripheral Initialization 
	pfSysSetCpuClock(&clkConfig);
	
	
	pfGpioInit(pfEduarmLEDGpioCfg,3);			// LED GPIO initialization 
	
	pfGpioInit(&pfEduarmEINT3GpioCfg,1);		// EINT3 GPIO initialization 
	
	pfGpioInit(pfEduarmUART0GpioCfg, 2);		// UART0 GPIO initialization 
	
	pfGpioInit(&pfEduarmADC0GpioCfg, 1); 		// ADC GPIO initialization 
	
	pfGpioInit(pfEduarmLCDGpioCfg,6);			// LCD GPIO initialization 

	pfGpioInit(pfEduarmTouchGpioCfg,6);			// Touch GPIO initialization 
	
	pfGpioInit(pfEduarmPUSHBUTTONGpioCfg,5);	// Pushbutton/keypad GPIO initialization 
	
	status = pfUart0Open(&uart0Config);			// UART0 GPIO initialization 
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	pfUart0WriteString("\r\n\r\nWelcome to Phi Education.");
	
	status = pfRitOpen(&ritConfig);
	if(status != enStatusSuccess)
	{
		DEBUG_WRITE("\r\nRIT initialization failed.");
		while(1);
	}
	else
		DEBUG_WRITE("\r\nRIT initialized.");
		
    pfRitStart();
	pfTickSetTimerPeriod(1);
	
	pfAdcOpen(&adcConfig);  // adc config initializaton
	if(status != enStatusSuccess)
	{
		DEBUG_WRITE("\r\nADC0 initialization failed.");
		while(1);
	}
	else
		DEBUG_WRITE("\r\nADC0 initialized.");
		
	
	status = pfEint3Open(&eInt3Config);		// initialization - EINT3 
	if(status != enStatusSuccess)
	{
		while(1);
	}
	status = pfSpi0Open((PFpCfgSpi0)&spi0Cfg);
	if(status != enStatusSuccess)
	{
		DEBUG_WRITE("\r\nSPI0 initialization failed.");
		while(1);
	}
	else
		DEBUG_WRITE("\r\nSPI0 initialized.");
	
	status  = gfxOpen(&lcdDisplayConfig);
	if(status != enStatusSuccess)
	{
		DEBUG_WRITE("\r\nLCD initialization failed.");
		while(1);
	}
	else
		DEBUG_WRITE("\r\nLCD initialized.");
		
	status = touchOpen(&touchConfig);
	if(status != enStatusSuccess)
	{
		DEBUG_WRITE("\r\nTouch panel initialization failed.");
		while(1);
	}
	else
		DEBUG_WRITE("\r\nTouch panel initialized.");
		
}

void eint3Callback(void)
{
	pfUart0WriteString("\r\nInside interrupt callback");
}
