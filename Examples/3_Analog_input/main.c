#include "windDSTHConfig.h"

int main()
{
	PFdword adcConvertedMvValue;
	PFword adcConvertedValue;
	PFbyte buff[5];

	boardInit();
	
	gfxFillRGB(WHITE);
	
	gfxDrawString(25, 30, "Analog Input Interface", enGfxFont_8X16, RED, WHITE);
	gfxDrawString(25, 95, "Digital Value", enGfxFont_8X16, RED, WHITE);
	gfxDrawRectangle(152,79 ,214,120);	//From Box 
	
	
 	gfxDrawString(25, 145, "Analog value(mv)", enGfxFont_8X16, RED, WHITE);
	gfxDrawRectangle(152,129 ,214,170);	//From Box 
	while(1)
		{
			pfAdcSingleConversion(enAdcChannel0, &adcConvertedValue);  //if pt varies value changes here
			pfItoA(adcConvertedValue, buff);	

			gfxFillArea(157,84 ,209,115,WHITE);
			gfxDrawString(170, 95,(const char*)buff, enGfxFont_8X16, RED, WHITE);
		
			
			pfAdcGetVoltageSingleConversion(enAdcChannel0, &adcConvertedMvValue);
			pfItoA(adcConvertedMvValue,buff);
			gfxFillArea(157,133 ,209,165,WHITE);
			gfxDrawString(170, 145,(const char*)buff, enGfxFont_8X16, RED, WHITE);
			pfTickDelayMs(500);
		}
		
return 0;
}