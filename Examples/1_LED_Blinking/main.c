#include "windDSTHConfig.h"

PFCfgGpio ledPins[] = {GPIO_PORT_1,GPIO_PIN_31,enGpioDirOutput,enGpioPinModePullUp,enGpioOpenDrainDisable,enGpioFunctionGpio};

int main()
{
	boardInit();
	pfGpioInit(ledPins, 1);	//GPIO pin initialze

	while(1)
	{
		pfGpioPinsSet(GPIO_PORT_1,GPIO_PIN_31 );
		pfTickDelayMs(1000);
		pfGpioPinsClear(GPIO_PORT_1,GPIO_PIN_31);
		pfTickDelayMs(1000);
	}
	return 0;
}