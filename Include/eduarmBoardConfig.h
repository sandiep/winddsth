#pragma once
#ifdef MCU_CHIP_lpc1768

extern PFCfgGpio pfEduarmLEDGpioCfg[3];			/** LED gpio pin configuration 	*/
extern PFCfgGpio pfEduarmPUSHBUTTONGpioCfg[5];	/** PUSHBUTTON gpio pin configuration */

extern PFCfgGpio pfEduarmUART0GpioCfg[2];		 /** UART0 gpio pin configuration */
extern PFCfgGpio pfEduarmUART2GpioCfg[2];        /** UART2 gpio pin configuration */
extern PFCfgGpio pfEduarmUART3GpioCfg[2];         /** UART3 gpio pin configuration */

extern PFCfgGpio pfEduarmEINT3GpioCfg;            /** EINT3 gpio pin configuration */
extern PFCfgGpio pfEduarmMma7660INTGpioCfg;          /** MMA7660  INT gpio pin configuration */
extern PFCfgGpio pfEduarmTouchINTGpioCfg;            /** Touch INT gpio pin configuration */

extern PFCfgGpio pfEduarmADC0GpioCfg;             /** ADC0 gpio pin configuration */
extern PFCfgGpio pfEduarmADC1GpioCfg;             /** ADC1 gpio pin configuration */
extern PFCfgGpio pfEduarmADC2GpioCfg;             /** ADC2 gpio pin configuration */
extern PFCfgGpio pfEduarmADC3GpioCfg;             /** ADC3 gpio pin configuration */
extern PFCfgGpio pfEduarmADC4GpioCfg;             /** ADC4 gpio pin configuration */
extern PFCfgGpio pfEduarmADC5GpioCfg;             /** ADC5 gpio pin configuration */

extern PFCfgGpio pfEduarmI2C1GpioCfg[2];          /** I2C1 gpio pin configuration */
extern PFCfgGpio pfEduarmI2C2GpioCfg[2];          /** I2C2 gpio pin configuration */


extern PFCfgGpio pfEduarmSSP0GpioCfg[4];          /** SSP0 gpio pin configuration */
extern PFCfgGpio pfEduarmTouchGpioCfg[6];          /** Touch gpio pin configuration */
extern PFCfgGpio pfEduarmSDcardGpioCfg[4];          /** SD Card gpio pin configuration */
extern PFCfgGpio pfEduarmLCDGpioCfg [6];			 /** LCD gpio pin configuration */

extern PFCfgGpio pfEduarmCAN1GpioCfg[2];          /** CAN1 gpio pin configuration */
extern PFCfgGpio pfEduarmCAN2GpioCfg[2];          /** CAN2 gpio pin configuration */


extern PFCfgGpio pfEduarmPWM1GpioCfg;             /** PWM1 gpio pin configuration */
extern PFCfgGpio pfEduarmPWM2GpioCfg;             /** PWM2 gpio pin configuration */
extern PFCfgGpio pfEduarmPWM3GpioCfg;             /** PWM3 gpio pin configuration */
extern PFCfgGpio pfEduarmPWM4GpioCfg;             /** PWM4 gpio pin configuration */
extern PFCfgGpio pfEduarmPWM5GpioCfg;             /** PWM5 gpio pin configuration */

extern PFCfgGpio pfEduarmTIMER0CAP1GpioCfg;     /** TIMER0CAP gpio pin configuration */
extern PFCfgGpio pfEduarmTIMER3CAP0GpioCfg;     /** TIMER3CAP gpio pin configuration */

extern PFCfgGpio pfHydraTIMER1MATGpioCfg[2];    /** TIMER1MAT gpio pin configuration */

extern PFCfgGpio pfEduarmI2SGpioCfg[5];          /** I2S gpio pin configuration */

PFEnBoolean isSwitchPressed(PFbyte* switchNumber);
#endif	// #ifdef MCU_CHIP_lpc1768