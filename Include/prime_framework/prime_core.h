/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Core Header File.
 *
 * 
 * Review status: NO
 *
 */ 
#pragma once

#include "prime_compiler.h"
#include "prime_types.h"
#include "prime_debug.h"
#include "prime_tick.h"
#include "prime_utils.h"
