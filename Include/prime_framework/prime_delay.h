/**
 * \file	prime_delay.h
 * \brief 	Prime Framework Delay API
 * \copyright Copyright (c) 2014 <br> PhiRobotics Research Pvt Ltd
 * 
 * \par
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module
 *
 * Review status: NO
 *
 */
#pragma once

/**
 * \ingroup core
 * \defgroup PF_DELAY DELAY
 * @{
 */

/** \brief if timer is configured for this delay file then use timer otherwise use approx delay fucntions */
#define TIMER_CONFIGURED 1

/**
 * Sets timer period for DELAY APIs
 * 
 * All calculations in the delay function will be done on basis of this timer period.
 * 
 * \param timerPeriod Time taken by timer to count from 0 to the value in compare match register.
 * \note If timer is configured, this function should be called before using delay.
 */
void pfDelaySetTimerPeriod(PFdword timerPeriod);

/**
 * Increments tick value by 1.
 * This function should be provided to timer as callback.
 */
void pfDelayTickUpdate(void);

/**
 * This will return timer Resolution in micro second
 *
 * \return timer resolution 
 */ 
PFdword pfDelayGetTimerPeriod(void);

/**
 * This will return delay tick 
 *
 * \return current Value of Delay-tick
 */
PFdword pfDelayGetTick(void);

/**
 * Resets tick value to zero
 */
void pfDelayTickReset(void);

/**
 * Set timeout value. 
 * This will calculate the number ticks required depending on the timer period set 
 * and returns the number number of ticks required till timeout
 *
 * \param time Timeout value in micro seconds.
 *
 * \return Value of \a tick which will occur at timeout
 */
PFdword pfDelaySetTimeout(PFdword time);

/**
 * The function compares \a time with the current \a tick value and decides if the timeout is over or not
 *
 * \param timeoutTick Timeout tick value. This value should be the one returned by \a  pfTickSetTimeoutMs function
 *
 * \return Timeout status in boolean.
 */
PFEnBoolean pfDelayCheckTimeout(PFdword timeoutTick);

/**
 * Micro second delay function.
 *
 * \param delayUs Delay value in micro seconds
 */ 
void pfDelayMicroSec(PFdword delayUs);

/**
 * Milli second delay function.
 *
 * \param delayMs Delay value in milli seconds
 */
void pfDelayMilliSec(PFdword delayMs);

/** @}	*/


