#include "prime_framework.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_tick.h"
#include "prime_uart0.h"
#include "prime_rit.h"
#include "prime_spi0.h"
#include "prime_adc.h"
#include "prime_eint3.h"
#include "graphics.h"
#include "touch.h"
#include "eduarmBoardConfig.h"

#ifdef PRIME_DEBUG
	#define DEBUG_WRITE(x) pfUart0WriteString(x) 
#else
	#define DEBUG_WRITE(x)
#endif

void boardInit(void);